//  Import dependencies
import React, { useEffect } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.min.css';

import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min';

import "./App.css";
// Import redux components
import { Provider } from "react-redux";
import store from "./store";
import logo from './images/logo.png';



// Import chat component
import Chat from "./components/chat/Chat";

// Import action
import { createSession } from "./actions/watson";

// Import axios
import axios from "axios";

if (localStorage.session) {
  delete axios.defaults.headers.common["session_id"];
  axios.defaults.headers.common["session_id"] = localStorage.session;
} else {
  delete axios.defaults.headers.common["session_id"];
}

// Connect application to redux
const App = () => {
  useEffect(() => {
    localStorage.clear();
    
    if (!localStorage.session) {
      // Create
      store.dispatch(createSession());
    }
  });
  return (
    <Provider store={store}>
      <header class="navbar navbar-expand-lg navbar-light bg-white">
        <div class="container">
          <a class="navbar-brand" href="#"><img src={logo} alt="Logo" width="90"/></a>
        </div>
      </header>
      
      <section className="bg_banner">        
        <div className="container">
          <div className="row mheight85vh align-item-center justify-content-md-between justify-content-end">
              <div className="col-md-6 text-black align-self-center">
                <h1 className="font-size5point5rem fontFamilyMontserrat-Black">Dermabot</h1>
                <h5 className="text-darkGray fontFamilyMontserrat-Regular">Hi I’m Dermabot, please ask me whatever you want to know about Dermatology. </h5>
              </div>
              <div className="col-lg-4 col-md-6 col-sm-8 text-black align-self-end mb-5 cursorPointer">
                {/* <div className="col-md-12 bg-white p-2 border-radiuspoint5 fontFamilyMontserrat-Medium text-orange d-flex align-items-center">
                  <i class="fas fa-robot border-end p-2"></i>
                  <p className="mb-0 p-2">Welcome to ask Nova</p>
                </div> */}
                <div class="accordion chatbotPosition col-md-12" id="accordionExample">
                  <div class="accordion-item overflow-hidden">
                      <h2 class="accordion-header" id="headingOne">
                        <button class="accordion-button collapsed fontFamilyCeraPro-Bold" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          <p className="mb-0 p-2 w-100"></p>
                          {/* Chat with Dermabot */}
                        </button>
                      </h2>
                      <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                        <Chat /> 
                        </div>
                      </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </section>
      



  



    </Provider>
  );
};

export default App;
