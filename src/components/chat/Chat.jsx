// import React, { useState, useEffect, useRef } from "react";
// import { connect } from "react-redux";

// //  Import action
// import { userMessage, sendMessage } from "../../actions/watson";

// const Chat = ({ chat, userMessage, sendMessage }) => {
//   // Handle Users Message
//   const [message, setMessage] = useState("");
//   const [loading, setLoading]  = useState(false);
//   const endOfMessages = useRef(null);

//   const scrollToBottom = () => {
//     endOfMessages.current.scrollIntoView({ behavior: "smooth" });
//   };
//   useEffect(scrollToBottom, [chat],connect());

//   //  Function that handles user submission
//   const handleClick = async (e) => {
//     const code = e.keyCode || e.which;

//     if (code === 13) {
//       console.log(message);
//       userMessage(message);
//       sendMessage(message);
//       setMessage("");
//     }
//   };

//   function connect(){
//     const exec = 1;
//     if(exec ==1)
//     sendMessage(message);
//     exec++;
    
//   }

//   return (
//     <div className="chat">
//       <h1 className="circle">Welcome To Ask Nova</h1>
//       <hr/>
//       {/* Handle Messages */}
//       <div class="historyContainer">
       
//         {chat.length === 0
//           ? ""
//           : chat.map((msg) => <div className={msg.type}>{msg.message}</div>)}
//         <div ref={endOfMessages}></div>
//       </div>
//       {/* Input Box */}
    
//       <input
//         style={{
//           width: "100%",
//           paddingLeft: "8px",
//           paddingTop: "6px",
//           paddingBottom: "6px",
//         }}
//         id="chatBox"
//         onChange={(e) => setMessage(e.target.value)}
//         onKeyPress={handleClick}
//         value={message}
//       ></input>
//     </div>
//   );
// };
// const mapStateToProps = (state) => ({
//   chat: state.watson.messages,
// });

// export default connect(mapStateToProps, { userMessage, sendMessage })(Chat);


import React, { useState, useEffect, useRef } from "react";
import { connect } from "react-redux";
import parse from 'html-react-parser';
import icon1 from '../../images/icon/icon1.png';
import $ from 'jquery';
//  Import action
import { userMessage, sendMessage } from "../../actions/watson";

const Chat = ({ chat, userMessage, sendMessage }) => {
  // Handle Users Message
  const [message, setMessage] = useState("");
  const endOfMessages = useRef(null);

 const fire = () =>{

  var trigger = true;
   if(trigger == true){
     sendMessage(message)
   }
   trigger = false;
 }
 const Btn_response = () =>{

  $(document).on("click","#listen",function(e) {
    
    var msg = e.target.getAttribute("value");
    setMessage(msg);
    userMessage(msg);
    sendMessage(msg);
    setMessage("");
    
});
 } 
  const scrollToBottom = () => {

    endOfMessages.current.scrollIntoView({ behavior: "smooth" });
    
  };
  useEffect(scrollToBottom, [chat]);
  useEffect(fire,[]);
  useEffect(Btn_response,[]);
  //  Function that handles user submission
  const handleClick = async (e) => {
    const code = e.keyCode || e.which;

    if (code === 13) {
      console.log(message);
      userMessage(message);
      sendMessage(message);
      setMessage("");
    }
  };

  return (
    <div className="chat">
      {/* Handle Messages */}
      <div class="historyContainer">
        <div className="row justify-content-center font-size14px">
          <div className="col-md-12"><p className="text-center mt-3 text-darkGray900">The very beginning</p></div>
          <div className="col-12">
            <hr />
          </div>
          <div className="col-md-4 col-4 mb-0 bg-white position-relative marginNag"><p className="text-center text-darkGray900">Today</p></div>
          <div className="col-md-12 text-center">
            <img src={icon1} alt="" className="mb-2"/>
            <p>Virtual Agent joined the conversation</p>
            </div>        
          
        </div>
        
        
        {/* <i class="fas fa-user-circle userIcon"></i> */}
            
        {chat.length === 0
          ? ""
          : chat.map(
            (msg) => <div className= {msg.type}> <div className="userIcon"></div> <div className="textbox p-3 font-size14px" dangerouslySetInnerHTML={{__html: msg.message}}></div></div>)}
        <div ref={endOfMessages}></div>


      
      </div>
      
      {/* Input Box */}
      <div className="input-group  input">
        <span class="input-group-text"><a href="#"><i class="fas fa-paperclip me-2 ps-2"></i></a></span>
        <input className="form-control fontFaimly_GothamBook bg-bg-transparent border-0 border-radius3"
              id="chatBox" placeholder="Type your message here"
              onChange={(e) => setMessage(e.target.value)}
              onKeyPress={handleClick}
              value={message}
            ></input> 
              <span class="input-group-text">
              <a href="#"><i class="fas fa-paper-plane pe-2"></i></a>
            </span>
      </div>
    </div>
  );
};
const mapStateToProps = (state) => ({
  chat: state.watson.messages,
});

export default connect(mapStateToProps, { userMessage, sendMessage })(Chat);
